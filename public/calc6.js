function updatePrice() {
  let s = document.getElementsByName("productType");
  let types = s[0];
  let price = 0;
  let prices = getPrices();
  let priceInd = parseInt(types.value)-1;
  if (priceInd >= 0) {
    price = prices.productTypes[priceInd];
  }
  let checkId = document.getElementById("checkboxes");
  checkId.style.display = (types.value == "3" ? "block" : "none");
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    if (checkbox.checked) {
      let propPrice = prices.productProperties[checkbox.name];
      if (propPrice !== undefined) {
        price += propPrice;
}
}
});
 let radioId = document.getElementById("radios");
 radioId.style.display = (types.value == "2" ? "block" : "none");
 let radios = document.getElementsByName("productOptions");
 radios.forEach(function(radio) {
    if (radio.checked) {
      let optionPrice = prices.productOptions[radio.value];
      if (optionPrice !== undefined) {
        price += optionPrice;
      }
    }
  });
  let q = document.getElementsByName("q");
  q = parseInt(q[0].value);
  let res = price*q;
  let prodPrice = document.getElementById("productPrice");
  prodPrice.innerHTML = res + " руб.";
}
function getPrices() {
  return {
    productTypes: [1000, 2500, 3000],
    productOptions: {
      option1: 100,
      option2: 70,
      option3: 120,
},
productProperties: {
      prop1: 5,
      prop2: 10,
      prop3: 15,
    }
  };
}
window.addEventListener("DOMContentLoaded", function(event) {
  let types = document.getElementsByName("productType");
  let radios = document.getElementsByName("productOptions");
  let checkboxes = document.querySelectorAll("#checkboxes input");
  let type = types[0];
  type.addEventListener("change", function(event) {
    radios.forEach(function(r){r.checked=false});
    checkboxes.forEach(function(r){r.checked=false});
    updatePrice();
  });
  radios.forEach(function(radio) {
  radio.addEventListener("change", function(event) {
  updatePrice();

    });
  });
  checkboxes.forEach(function(checkbox) {
  checkbox.addEventListener("change", function(event) {
  updatePrice();
    });
  });
  let quantity = document.getElementsByName("q");
  let q = quantity[0];
  q.addEventListener("change", function(event){
    updatePrice();
  });
  updatePrice();
});
